@extends('templates.base')

@section('conteudo')
    <main>
        <h1>Teoria</h1>
        <hr>
        <h2>Descrição das pilhas:</h2>
        
        <p>
            As baterias são conjuntos de pilhas ligadas em série, ou seja, são dispositivos eletroquímicos nos quais ocorrem reações de oxidorredução, produzindo uma corrente elétrica. Podem ser chamadas ainda de pilhas secundárias, baterias secundárias ou acumuladores.
        </p>

        <p>
            Aqui embaixo podemos ver alguns modelos de baterias usadas no dia a dia:
        </p>

        <img class="bateria" src="../imgs/bateria9v.jpeg" alt="bateria">
        <img class="bateria" src="../imgs/bateria1.jpeg" alt="bateria">
        <img class="bateria" src="../imgs/bateria2.jpeg" alt="bateria">
        <img class="bateria" src="../imgs/bateria3.jpeg" alt="bateria">

        <p>

        </p>
        <p>
            Uma curiosidade: Você sabia que as primeiras baterias conhecidas da história foram criadas há mais de 2.000 anos? Conhecidas como as "Pilhas de Bagdá", elas foram descobertas em 1938, perto de Bagdá, no Iraque. Essas pilhas, datadas do século I a.C., consistiam em vasos de argila com um eletrodo de cobre no interior e um eletrodo de ferro no exterior. Entre os eletrodos, havia um separador de argila e um líquido eletrólito, provavelmente uma solução ácida ou alcalina. Embora não tenha sido comprovado que essas pilhas foram usadas para fins práticos, a ideia de que as pessoas da antiguidade já haviam descoberto e experimentado com tecnologia de baterias é surpreendente.
        </p>
        
    </main>
    @endsection

@section('rodape')
<h4>Rodapé Teoria</h4>
@endsection